# README #

### How to run script ###

* python3 convert.py <folder_name>

### Description ###

* Converts local position from Cartesian coordinates to geographic coordinate system
* There must be a folder with two csv files, one with local frame reference and another with list of local positions
* Results are saved to <folder_name>/<folder_name>local_position_coordinates.csv

### Folder Tree Example ###

```bash
├── folder1
│   ├── local_frame_ref.csv
│   ├── local_position.csv
└── convert.py
```

### How to run previous example ###

* python3 convert.py folder1
