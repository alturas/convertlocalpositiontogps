import csv
from numpy import sin, cos, radians, std, mean
import math
import sys

BLADE = 2
SIDE = 3
LAT = 5
LONG=6
ALT = 7
PITCH = 8
YAW = 10
DISTANCE_TO_BLADE = -1

R = 6.371 * 1e6


def get_distance_between_two_points(p1: list, p2: list) -> float:
    """Computes the distance between two points """
    delta_x = (p2[0] - p1[0]) * (p2[0] - p1[0])
    delta_y = (p2[1] - p1[1]) * (p2[1] - p1[1])
    delta_z = (p2[2] - p1[2]) * (p2[2] - p1[2])
    return math.sqrt(delta_x + delta_y + delta_z)

def gps_to_cartesian(gps: list) -> list:
    x = (R + gps[2]) * cos(gps[0]) * cos(gps[1]) # Greenwich
    y = (R + gps[2]) * cos(gps[0]) * sin(gps[1]) # direct triangle
    z = (R + gps[2]) * sin(gps[0]) #north pole
    
    return([x, y, z])

def pose_to_cartesian(pitch: float, yaw: float, d: float):
    x = d * sin(pitch) 
    y = d * cos(pitch) * sin(yaw)
    z = d * cos(pitch) * cos(yaw)
    return([x, y, z])


# def update_gps_with_drone_pose(gps:list, yaw: float, pitch: float, d:float):
#     aditional_latitude = arctan(d * cos(yaw) / (R + gps[2])) * -cos(pitch)
#     gps[0] +=  aditional_latitude
#     gps[1] += arctan(d * sin(yaw) / (R + gps[2])) # longitude
#     gps[2] += d*sin(aditional_latitude) + d * sin(pitch)
#     return gps

def get_distance_from_two_photos(photos: list, use_pose: bool):
    points = []
    for photo in photos:
        gps =[radians(float(photo[LAT])), radians(float(photo[LONG])), float(photo[ALT])]
        yaw = radians(float(photo[YAW]))
        pitch = radians(float(photo[PITCH]))
        d = float(photo[DISTANCE_TO_BLADE])

        if use_pose:
            point = [sum(coordinate) for coordinate in zip(gps_to_cartesian(gps), pose_to_cartesian(pitch, yaw, d))]
        else:
            point = gps_to_cartesian(gps)
        points.append(point)
    return get_distance_between_two_points(points[0], points[1])


def main():
    file_name = sys.argv[1]
    if len(sys.argv) == 3 and sys.argv[2] == '0':
        use_pose =False
        print("Using just GPS data")
    else:
        print("Using GPS data and photo pose")
        use_pose = True

    with open(file_name, 'r') as file:
        reader = csv.reader(file)
        next(reader)

        previous_id = None
        lengths = []
        for row in reader:
            id = f"{row[BLADE]}_{row[SIDE]}"
            if id != previous_id:
                if previous_id is not None:

                    distance = get_distance_from_two_photos([first_photo, last_photo], use_pose)
                    lengths.append(distance)
                    print(f"{previous_id}: {distance}")
                first_photo =row             

            previous_id = id
            last_photo = row

        file.close()

        distance = get_distance_from_two_photos([first_photo, last_photo], use_pose) #Last side of file
        print(f"{previous_id}: {distance}")
        lengths.append(distance)

        rsd = std(lengths)/mean(lengths)*100
        print(f"mean: {mean(lengths)}, rsd:{rsd}")
        


if __name__ == "__main__":
    main()