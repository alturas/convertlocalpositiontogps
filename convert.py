import csv
import numpy as np
import sys

file_name = sys.argv[1]
with open(file_name + '/local_frame_ref.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)
    for row in reader:
        lat=float(row[6])
        long=float(row[7])
        alt=float(row[8])
    file.close()
    
with open(file_name + '/' + file_name + '_local_position_coordinates.csv', 'w') as f2:
    writer = csv.writer(f2)
    header = ['latitude', 'longitude', 'altitude']
    writer.writerow(header)
    k = 111111
    with open(file_name + '/local_position.csv', 'r') as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            x=float(row[4])
            y=float(row[5])
            z=float(row[6])
            new_lat = lat+y/k
            data=[new_lat, long-x/(k*np.cos(np.radians(new_lat))), alt+z]
            writer.writerow(data)
        file.close()
    f2.close()
